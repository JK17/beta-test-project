/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FormContentByIdQuery
// ====================================================

export interface FormContentByIdQuery_content_formContent {
  __typename: "FormContent";
  title: string | null;
  subtitle: string | null;
  buttonText: string | null;
  submitted: string | null;
}

export interface FormContentByIdQuery_content {
  __typename: "Content";
  title: string | null;
  meta: any | null;
  data: any | null;
  formContent: FormContentByIdQuery_content_formContent | null;
}

export interface FormContentByIdQuery {
  content: FormContentByIdQuery_content | null;
}

export interface FormContentByIdQueryVariables {
  id: string;
}
