/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ContentSelectView
// ====================================================

export interface ContentSelectView {
  __typename: "Content";
  id: string | null;
  title: string | null;
  slug: string | null;
}
