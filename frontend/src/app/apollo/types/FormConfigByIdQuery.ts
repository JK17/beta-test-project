/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FormConfigByIdQuery
// ====================================================

export interface FormConfigByIdQuery_formConfig {
  __typename: "FormConfig";
  id: string | null;
  name: string | null;
  config: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface FormConfigByIdQuery {
  formConfig: FormConfigByIdQuery_formConfig | null;
}

export interface FormConfigByIdQueryVariables {
  id: string;
}
