/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ModelsInTownQuery
// ====================================================

export interface ModelsInTownQuery_content {
  __typename: "Content";
  title: string | null;
  meta: any | null;
}

export interface ModelsInTownQuery_modelsInTown_modelContent_images {
  __typename: "File";
  id: string | null;
  metadata: any | null;
}

export interface ModelsInTownQuery_modelsInTown_modelContent {
  __typename: "ModelContent";
  name: string | null;
  country: string | null;
  height: number | null;
  bust: number | null;
  waist: number | null;
  hips: number | null;
  shoes: number | null;
  eyeColor: string | null;
  hairColor: string | null;
  instagram: string | null;
  images: (ModelsInTownQuery_modelsInTown_modelContent_images | null)[] | null;
}

export interface ModelsInTownQuery_modelsInTown {
  __typename: "Content";
  id: string | null;
  slug: string | null;
  modelContent: ModelsInTownQuery_modelsInTown_modelContent | null;
}

export interface ModelsInTownQuery {
  content: ModelsInTownQuery_content | null;
  modelsInTown: (ModelsInTownQuery_modelsInTown | null)[];
}

export interface ModelsInTownQueryVariables {
  id: string;
}
