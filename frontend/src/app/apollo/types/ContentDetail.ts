/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentDetail
// ====================================================

export interface ContentDetail_content_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
}

export interface ContentDetail_content {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  contentType: ContentDetail_content_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ContentDetail {
  content: ContentDetail_content | null;
}

export interface ContentDetailVariables {
  id: string;
}
