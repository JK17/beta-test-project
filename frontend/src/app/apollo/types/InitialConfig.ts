/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: InitialConfig
// ====================================================

export interface InitialConfig_contents_contentType {
  __typename: "ContentType";
  name: string | null;
  slug: string | null;
}

export interface InitialConfig_contents {
  __typename: "Content";
  id: string | null;
  slug: string | null;
  contentType: InitialConfig_contents_contentType | null;
}

export interface InitialConfig_settings {
  __typename: "Setting";
  id: string | null;
  value: string | null;
}

export interface InitialConfig {
  contents: (InitialConfig_contents | null)[];
  settings: (InitialConfig_settings | null)[];
}
