/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: LoadOldDataMutation
// ====================================================

export interface LoadOldDataMutation_loadOldData {
  __typename: "OldContent";
  title: string | null;
  alias: string | null;
  short_desc: string | null;
  desc: string | null;
  image: string | null;
  gallery_id: number | null;
  manufacturer_id: number | null;
  metakey: string | null;
  metadesc: string | null;
  ordering: number | null;
  created: any | null;
  published: boolean | null;
  checked_out: number | null;
  checked_out_time: any | null;
  recommended: boolean | null;
  hits: number | null;
  default_image: string | null;
  in_town: boolean | null;
  new_face: boolean | null;
  height: string | null;
  bust: string | null;
  waist: string | null;
  hips: string | null;
  shoe_size: string | null;
  hair_colour: number | null;
  eye_colour: number | null;
  race: number | null;
  pdf: string | null;
}

export interface LoadOldDataMutation {
  loadOldData: (LoadOldDataMutation_loadOldData | null)[];
}

export interface LoadOldDataMutationVariables {
  input?: string | null;
}
