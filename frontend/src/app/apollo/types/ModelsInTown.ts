/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ModelsInTown
// ====================================================

export interface ModelsInTown_content {
  __typename: "Content";
  title: string | null;
  meta: any | null;
}

export interface ModelsInTown_modelsInTown_modelContent_images {
  __typename: "File";
  id: string | null;
  metadata: any | null;
}

export interface ModelsInTown_modelsInTown_modelContent {
  __typename: "ModelContent";
  name: string | null;
  country: string | null;
  images: (ModelsInTown_modelsInTown_modelContent_images | null)[] | null;
}

export interface ModelsInTown_modelsInTown {
  __typename: "Content";
  id: string | null;
  modelContent: ModelsInTown_modelsInTown_modelContent | null;
}

export interface ModelsInTown {
  content: ModelsInTown_content | null;
  modelsInTown: (ModelsInTown_modelsInTown | null)[];
}

export interface ModelsInTownVariables {
  id: string;
}
