/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: Files
// ====================================================

export interface Files_files {
  __typename: "File";
  id: string | null;
  metadata: any | null;
}

export interface Files {
  files: (Files_files | null)[] | null;
}

export interface FilesVariables {
  ids: (string | null)[];
}
