import { BrowserModule } from '@angular/platform-browser';
import {
  APP_ID,
  APP_INITIALIZER,
  Inject,
  NgModule,
  PLATFORM_ID,
} from '@angular/core';

import { AppComponent } from '@app/app.component';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AppRoutingModule} from '@app/app.routes';
import {LoggerModule, NgxLoggerLevel} from 'ngx-logger';
import {ErrorService} from '@app/error.service';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {LayoutModule} from '@app/layout/layout.module';
import {ContentModule} from '@app/content/content.module';
import {AppConfigService} from '@app/app-config.service';
import {CommonModule, isPlatformBrowser} from '@angular/common';
import {TwpApolloModule} from '@app/apollo/apollo.module';
import {CookieService} from "ngx-cookie-service";
import {WINDOW_PROVIDERS} from "@app/window.service";
import {FormlyModule} from "@ngx-formly/core";
import {FormlyBootstrapModule} from "@ngx-formly/bootstrap";
import {FORMLY_CONFIG, FormlyTypesModule} from "@app/shared/formly/formly-types.module";
import { ResponsiveService } from '@app/responsive.service';
import {SelectionCookieService} from "@app/shared/services/selection-cookie.service";
import {ToastrModule} from "ngx-toastr";
import {ModelSelectComponent} from "@app/layout/navbar/model-select.component";
import {NgSelectModule} from "@ng-select/ng-select";

export function appConfigFactory(config: AppConfigService) {
  return () => config.load();
}

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserAnimationsModule,
    LoggerModule.forRoot({
      level: NgxLoggerLevel.DEBUG,
      serverLogLevel: NgxLoggerLevel.OFF,
    }),
    AppRoutingModule,
    BrowserModule.withServerTransition({ appId: 'bertaModels' }),
    FormsModule,
    ReactiveFormsModule,
    TwpApolloModule,
    LayoutModule,
    ContentModule,
    CommonModule,
    FormlyTypesModule,
    FormlyModule.forRoot(FORMLY_CONFIG),
    FormlyBootstrapModule,
    ToastrModule.forRoot()
  ],
  providers: [
    ErrorService,
    CookieService,
    AppConfigService,
    ResponsiveService,
    SelectionCookieService,
    WINDOW_PROVIDERS,
    {
      provide: APP_INITIALIZER,
      useFactory: appConfigFactory,
      deps: [AppConfigService],
      multi: true,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(
    @Inject(PLATFORM_ID) private platformId: Object,
    @Inject(APP_ID) private appId: string,
  ) {
    const platform = isPlatformBrowser(platformId)
      ? 'in the browser'
      : 'on the server';
    console.log(`Running ${platform} with appId=${appId}`);
  }
}
