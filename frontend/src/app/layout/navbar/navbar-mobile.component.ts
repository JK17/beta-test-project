import { Component, Input, Output, EventEmitter } from '@angular/core';
import { transition, trigger, style, animate } from '@angular/animations';
import { NavigationEnd, Router } from "@angular/router";

@Component({
  selector: 'navbar-mobile',
  template: `
    <div class="is-flex">
      <div
        class="hamburger"
        [ngClass]="isCollapsed ? null : 'opened'"
        (click)="toggleMenu()"
      >
        <div class="first"></div>
        <div class="second"></div>
        <div class="third"></div>
      </div>
      <a routerLink="/my-selection">
        <img
          *ngIf="isCollapsed"
          src="/assets/img/heart.svg"
          class="heart"
        />
      </a> 
      <span *ngIf="isCollapsed" class="fav-counter">({{ numFavourites }})</span>
    </div>
    <menu-mobile *ngIf="!isCollapsed" @fade>
      <ng-content></ng-content>
    </menu-mobile>
    <img src="/assets/img/logo.svg" class="logo" routerLink="/"/>
  `,
  styleUrls: ['./navbar-mobile.component.sass'],
  animations: [
    trigger('fade', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('.5s', style({ opacity: 1 })),
      ]),
      transition(':leave', [animate('.2s', style({ opacity: 0 }))]),
    ]),
  ],
})
export class NavbarMobileComponent {

  @Input() numFavourites: number;
  isCollapsed = true;
  @Output()
  switchMenu = new EventEmitter<boolean>();

  constructor(private readonly router: Router) {

    // Detect route changes to ensure menu-mobile is collapsed
    router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.isCollapsed = true;
      }
    })

  }

  toggleMenu() {
    this.isCollapsed = !this.isCollapsed;
    this.switchMenu.emit(this.isCollapsed);
  }

}
