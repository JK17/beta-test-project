import { NgModule } from '@angular/core';

import { RouterModule, Scroll } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FooterComponent } from '@app/layout/footer/footer.component';
import { IconsModule } from '@app/icons.module';
import { NavbarComponent } from '@app/layout/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutComponent } from '@app/layout/layout.component';
import { LayoutService } from '@app/layout/layout.service';
import { NavbarDesktopComponent } from '@app/layout/navbar/navbar-desktop.component';
import { NavbarMobileComponent } from '@app/layout/navbar/navbar-mobile.component';
import { MenuMobileComponent } from '@app/layout/navbar/menu-mobile.component';
import { ScrollUpComponent } from '@app/layout/scroll-up.component';
import { NgSelectModule } from "@ng-select/ng-select";
import { ModelSelectComponent } from "@app/layout/navbar/model-select.component";
import { LegalModalComponent } from './legal-modal/legal-modal.component';

@NgModule({
  imports: [RouterModule, CommonModule, IconsModule, BrowserAnimationsModule, NgSelectModule],
  declarations: [
    LayoutComponent,
    FooterComponent,
    NavbarComponent,
    NavbarDesktopComponent,
    NavbarMobileComponent,
    MenuMobileComponent,
    ScrollUpComponent,
    ModelSelectComponent,
    LegalModalComponent
  ],
  entryComponents: [LayoutComponent],
  providers: [LayoutService],
  exports: [FooterComponent, ScrollUpComponent],
})
export class LayoutModule { }
