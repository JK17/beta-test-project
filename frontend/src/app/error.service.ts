import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {HttpErrorResponse} from '@angular/common/http';
import {NGXLogger} from 'ngx-logger';


@Injectable()
export class ErrorService {

  constructor(private readonly toastr: ToastrService,
              private readonly logger: NGXLogger){}

  handleError(err?: Error) {
    if (!err) return;

    this.logger.error('Error service', err);

    const {toastr} = this;

    if (err instanceof HttpErrorResponse) {

      if (!navigator.onLine) return toastr.error('No Internet Connection');

      const {status} = err;

      if (status === 0) return toastr.error('Cannot connect to API server');
      if (status >= 500) return toastr.error('An unexpected error has occurred on the server');

    }

  }

}
