import {animate, animation, query, stagger, state, style, transition, trigger} from '@angular/animations';


export const fadeAnimation =
  trigger('fade', [

    state('in', style({ opacity: 1 })),
    state('out', style({ opacity: 0 })),

    transition('void => in', [
      style({ opacity: 0 }),
      animate('{{duration}} {{delay}} ease-out')
    ], { params: { duration: '1s', delay: '100ms' }}),

    transition('void => out', [
      style({ opacity: 1 }),
      animate('{{duration}} {{delay}} ease-out')
    ], { params: { duration: '1s', delay: '100ms' }}),

    transition('in => out', [
      animate('{{duration}} ease-out')
    ], { params: { duration: '1s' }}),

    transition('out => in', [
      animate('1s ease-out')
    ], { params: { duration: '1s' }})

  ]);

export const fadeItemsAnimation =
  trigger('fadeItems', [

    transition(':enter', [

      query('.fade-item', [
        style({opacity: 0}),
        stagger(500, [
          animate('{{duration}} {{delay}} ease-out', style({opacity: 1}))
        ])
      ])

    ], { params: { duration: '500ms', delay: '500ms'}}),

    transition(':leave', [
      animate('{{duration}} {{delay}} ease-out', style({ opacity: 0 }))
    ], { params: { duration: '500ms', delay: '500ms' }})

  ]);
