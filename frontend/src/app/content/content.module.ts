import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { IconsModule } from '../icons.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { LayoutModule } from '../layout/layout.module';
import { ModelContentResolver } from '@app/content/resolvers/model-content.resolver';
import { ModelContentComponent } from '@app/content/model-content/model-content.component';
import { ModelsInTownComponent } from '@app/content/models-in-town/models-in-town.component';
import { ModelsInTownResolver } from '@app/content/resolvers/models-in-town.resolver';
import { ModelsByGenderResolver } from '@app/content/resolvers/models-by-gender.resolver';
import { ModelsByGenderComponent } from '@app/content/models-by-gender/models-by-gender.component';
import { FormResolver } from '@app/content/resolvers/form.resolver';
import { FormComponent } from '@app/content/form/form.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { FormlyModule } from '@ngx-formly/core';
import { NgbAlertModule } from '@ng-bootstrap/ng-bootstrap';
import { MySelectionComponent } from '@app/content/my-selection/my-selection.component';
import { ContentResolver } from '@app/content/resolvers/content.resolver';
import { FloatingMenuComponent } from '@app/content/floating-menu/floating-menu.component';
import { ModelBoxComponent } from '@app/content/model-box/model-box.component';

@NgModule({
  imports: [
    RouterModule,
    CommonModule,
    IconsModule,
    BrowserAnimationsModule,
    LayoutModule,
    ReactiveFormsModule,
    FormlyModule,
    NgbAlertModule,
    FormsModule
  ],
  declarations: [
    ModelContentComponent,
    ModelsInTownComponent,
    ModelsByGenderComponent,
    FormComponent,
    MySelectionComponent,
    FloatingMenuComponent,
    ModelBoxComponent,
  ],
  entryComponents: [
    ModelContentComponent,
    ModelsInTownComponent,
    ModelsByGenderComponent,
    FormComponent,
    MySelectionComponent,
  ],
  providers: [
    ModelContentResolver,
    ModelsInTownResolver,
    ModelsByGenderResolver,
    FormResolver,
    ContentResolver,
  ],
})
export class ContentModule { }
