import {Component, Input, OnInit} from '@angular/core';
import {SelectionCookieService} from "@app/shared/services/selection-cookie.service";
import {ModelContentDetail_content} from "@app/apollo/types/ModelContentDetail";

@Component({
  selector: 'model-box',
  templateUrl: 'model-box.component.html',
  styleUrls: ['model-box.component.sass'],
})
export class ModelBoxComponent implements OnInit {
  @Input() model: ModelContentDetail_content;

  selected: boolean;

  constructor(
    private cookieService: SelectionCookieService
  ) {}

  ngOnInit(): void {
    const selectedIds = this.cookieService.getSelectedIds();
    this.selected = selectedIds.indexOf(this.model.id) > -1;
  }

  urlBgImg = img => `url(${img})`;

  onHeartClicked() {
    this.selected = !this.selected;

    const {id} = this.model;

    this.selected ?
      this.cookieService.addSelectedId(id) :
      this.cookieService.removeSelectedId(id);

  }
}
