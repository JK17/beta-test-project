import { Observable } from 'rxjs/Observable';
import { ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { modelContentDetailQuery } from '@app/app.graphql';
import { map } from 'rxjs/operators';
import { ModelContentDetailView } from '@app/apollo/types/ModelContentDetailView';
import { ModelContentDetail } from '@app/apollo/types/ModelContentDetail';

@Injectable()
export class ModelContentResolver implements Resolve<ModelContentDetailView> {

  constructor(private readonly apollo: Apollo){}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<ModelContentDetailView> {
    // TODO error handling

    const {contentId} = route.data;

    return this.apollo.query<ModelContentDetail>({
      query: modelContentDetailQuery,
      variables: { id: contentId },
    }).pipe(map(({data}) => data.content));

  }



}
