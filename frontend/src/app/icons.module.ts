import {NgModule} from '@angular/core';
import {FontAwesomeModule} from '@fortawesome/angular-fontawesome';

import {library} from '@fortawesome/fontawesome-svg-core';

import { faFacebookSquare } from '@fortawesome/free-brands-svg-icons';
import {faAngleLeft, faArrowUp, faCalendar, faSave, faSpinner} from '@fortawesome/free-solid-svg-icons';

library.add(
  faFacebookSquare, faAngleLeft, faSave, faCalendar, faSpinner, faArrowUp
);

@NgModule({
  imports: [
    FontAwesomeModule
  ],
  exports: [
    FontAwesomeModule
  ]
})
export class IconsModule {}
