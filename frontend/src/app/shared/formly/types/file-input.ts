import {FieldType} from "@ngx-formly/core";
import {ChangeDetectorRef, Component, ElementRef, ViewChild} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs/Observable";
import {Subject} from "rxjs/Subject";
import {Subscription} from 'rxjs/Subscription';
import {filter, map} from 'rxjs/operators';
import {BehaviorSubject} from 'rxjs/BehaviorSubject';
import {Apollo} from 'apollo-angular';
import {environment} from '@src/environments/environment';
import {FileById} from "@app/apollo/types/FileById";
import {fileByIdQuery} from "@app/app.graphql";
import {FileDetailView} from "@app/apollo/types/FileDetailView";

@Component({
  selector: 'formly-file-input',
  template: `
    <div class="custom-file-input-container">
      <input type="hidden" [formlyAttributes]="field" [formControl]="formControl">

      <ng-container [ngSwitch]="(state$ | async)">

        <input *ngSwitchCase="'ready'"
               type="file"
               #fileInput
               class="d-none"
               [accept]="to.accept" 
               (change)="onChange($event)">
        
        <button *ngSwitchCase="'ready'" class="btn file-upload-btn" type="button" (click)="selectFile()">
          {{ to.buttonText }} <fa-icon [icon]="'arrow-up'"></fa-icon>
        </button>

        <ng-container *ngSwitchCase="'generating_preview'">
          Generating preview...
          <fa-icon icon="spinner" fixedWidth="true" [spin]="true"></fa-icon>
        </ng-container>

        <ng-container *ngSwitchCase="'uploading'">
          Uploading...
          <fa-icon icon="spinner" fixedWidth="true" [spin]="true"></fa-icon>
        </ng-container>

        <ng-container *ngSwitchCase="'uploaded'">
          
          <ng-container [ngSwitch]="resourceType">
            
            <ng-container *ngSwitchCase="'video'">
              
              <video [poster]="previewUrl" controls width="800">
                <source *ngFor="let source of sources" [src]="source.url" [type]="source.type"/> 
              </video>
              
            </ng-container>

            <ng-container *ngSwitchCase="'image'">
                
              <img [src]="previewUrl" class="img-fluid" alt="Image Preview">
              
            </ng-container>
            
          </ng-container>

          <button type="button" class="close" style="opacity: 0.7" aria-label="Close" (click)="onDelete()">
            <span aria-hidden="true">&times;</span>
          </button>

        </ng-container>

        <ng-container *ngSwitchCase="'error'">
          {{ error }}
        </ng-container>

      </ng-container>

    </div>
  `
})
export class FormlyFileInput extends FieldType {

  @ViewChild('fileInput') fileInput: ElementRef;

  previewUrl: string;
  error: Error;

  value$: Observable<any>;

  state$: BehaviorSubject<string>;

  resourceType: string;
  sources: { type: string, url: string }[];

  private subs: Subscription[];

  constructor(private http: HttpClient,
              private apollo: Apollo,
              private changeDetector: ChangeDetectorRef) {
    super();
  }

  ngOnInit(): void {

    super.ngOnInit();

    this.subs = [];
    this.state$ = new BehaviorSubject<string>('ready');

    const {formControl, subs} = this;

    this.value$ = formControl.valueChanges;

    subs.push(
      this.value$
        .pipe(filter(value => !value))
        .subscribe(_ => this.onReady())
    );

    subs.push(
      this.value$
        .pipe(filter(value => !!value))
        .subscribe(value => this.generatePreview(value))
    );

    // initial state

    const initialValue = formControl.value;

    if (initialValue) {
      this.generatePreview(initialValue);
    } else {
      this.onReady();
    }
  }

  ngOnDestroy(): void {
    super.ngOnDestroy();
    this.subs.forEach(s => s.unsubscribe());
  }

  onReady() {
    this.previewUrl = null;
    this.state$.next('ready');
    this.changeDetector.markForCheck();
  }

  onDelete() {
    this.formControl.setValue(null);
  }

  selectFile() {
    this.fileInput.nativeElement.click();
  }

  generatePreview(id: number) {

    const { state$ } = this;

    state$.next('generating_preview');

    this.subs.push(

      this.apollo.query<FileById>({
        query: fileByIdQuery,
        variables: { id }
      }).subscribe(
        ({ data }) => {

          const { file } = data;
          const { mimetype } = file;

          // TODO fix this hack for how metadata has string type instead of object
          const metadata = JSON.parse(JSON.stringify(file.metadata));

          const [ resourceType ] = mimetype.split('/');

          switch(resourceType) {

            case 'video':

              this.previewUrl = metadata.url_preview;

              this.sources = [
                { type: 'video/mp4', url: metadata.url_mp4 },
                { type: 'video/webm', url: metadata.url_webm },
                { type: 'video/ogg', url: metadata.url_ogg },
              ];

              break;


            case 'image':

              this.previewUrl = metadata.secure_url;

              break;
          }


          this.resourceType = resourceType;

          state$.next('uploaded');

        },
        err => {
          this.error = err;
          state$.next('error');
        },
        () => {
          // force detection of changes made from the subscription context
          this.changeDetector.markForCheck();
        }
      )
    );

  }

  onChange(event: any) {

    const {files} = event.target;

    if (files.length) {

      const [file] = files;

      this.state$.next('uploading');

      this.subs.push(
        this.uploadFile(file)
          .subscribe(
            file => this.formControl.setValue(file.id),
            err => {
              this.error = err;
              this.state$.next('error');
            }
          )
      );

    }

  }

  uploadFile(file: File): Observable<FileDetailView> {

    const reader = new FileReader();
    reader.readAsDataURL(file);

    let result = new Subject<FileDetailView>();

    reader.onabort = () => {
      result.error(new Error('Reader was aborted'));
    };

    reader.onerror = () => {
      result.error(reader.error);
    };

    reader.onload = () => {

      const formData = new FormData();
      formData.append('file', file, file.name);

      const url = environment.apiUrl + '/file';

      this.subs.push(
        this.http.post(url, formData)
          .pipe(map(resp => resp as FileDetailView))
          .subscribe(
            res => {
              result.next(res);
              result.complete();
            },
            err => result.error(err)
          )
      );

    };

    return result;
  }

}
