import {Component} from '@angular/core';
import {FieldType} from '@ngx-formly/core';
import {Observable} from 'rxjs/Observable';
import {of} from 'rxjs';


@Component({
  selector: 'formly-field-select',
  template: `
    <ng-select class="custom"
               [formlyAttributes]="field"
               [formControl]="formControl"
               [items]="items$ | async"
               [clearable]="!to.required"
               [placeholder]="to.placeholder"
               bindValue="id"
               bindLabel="name">
    </ng-select>
  `
})
export class FormlyFieldSelect extends FieldType {

  items$: Observable<any[]>;

  ngOnInit(): void {
    this.items$ = this.to.select.items$ || of(this.to.select.items);
    super.ngOnInit();
  }


}

