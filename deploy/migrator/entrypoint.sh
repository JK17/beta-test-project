#!/bin/sh -e

. /usr/local/bin/env_secrets_expand.sh

echo "NODE_ENV = $NODE_ENV"

if [ "${NODE_ENV}" == "production" ]; then

  # migrate up only
  npm run migrate up

else

  npm run migrate up
  npm run migrate up:test

  # reset the entire database
  npm run migrate:redo

fi
