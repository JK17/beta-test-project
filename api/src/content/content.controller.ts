import {Controller, Get, Param, Response, Request} from '@nestjs/common';
import {ContentService} from "@app/content/content.service";
import {FileService} from "@app/file/file.service";

const request = require('request');

@Controller('content')
export class ContentController {

    constructor(private contentService: ContentService,
                private fileService: FileService){}

    @Get(':id/pdf')
    async generateAndDownloadPdf(@Param('id') id: number,
                                 @Response() res) {

        const model = await this.contentService.findById(id);

        const imageIds = model.data.images.map(image => image.id).splice(0, 2);

        const images = await this.fileService.findByIds(imageIds);

        if (images.length === 1) { // Duplicate first image if second image not present
            images.push(images[0]);
        }

        const html = this.contentService.generatePdfHtml(model.data, images);

        const options = {
            method: 'POST',
            url: 'http://wkhtmltopdf/pdf',
            formData: {
                html: html,
                'options[viewport-size]': '1123x793',
                'options[orientation]': 'Landscape',
                'options[margin-bottom]': '0',
                'options[margin-left]': '0',
                'options[margin-right]': '0',
                'options[margin-top]': '0',
                'options[encoding]': 'UTF8'
            },
            encoding: null
        };

        const pdf = await new Promise(function (resolve, reject) {

            request(options, function (error, response, body) {
                if (error) return reject(error);
                resolve(body);
            });

        });

        res.setHeader('Content-Type', 'application/pdf');
        res.setHeader('Content-Disposition', `attachment; filename="${model.slug}.pdf"`);
        res.send(pdf);

    }

}
