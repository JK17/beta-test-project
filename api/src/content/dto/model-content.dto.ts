import { assignClean } from '@app/shared/utils';
import { ModelContent } from '@app/graphql/schema';


export class ModelContentDto extends ModelContent {

    constructor(data: any) {
        super();
        assignClean(this, data);
    }

}
