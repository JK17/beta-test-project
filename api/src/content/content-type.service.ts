import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {ContentTypeEntity} from '../orm/entities/content-type.entity';
import {Repository} from 'typeorm';
import {QueryFilter} from '../shared/query-filter';

@Injectable()
export class ContentTypeService {

    constructor(
        @InjectRepository(ContentTypeEntity)
        private readonly contentTypeRepository: Repository<ContentTypeEntity>
    ){}

    async find(filter: QueryFilter): Promise<ContentTypeEntity[]> {

        const { offset, limit, q, name} = filter;

        const builder = this.contentTypeRepository
            .createQueryBuilder('contentType');

        if (q) {
            builder
                .where('contentType.name ~* :query')
                .setParameters({ query: `.*${q}.*` });
        }

        if(name) {
            builder
                .where('contentType.name = :name')
                .setParameters({ name })
        }

        return await builder
            .offset(offset)
            .limit(limit)
            .orderBy('id', 'ASC')
            .getMany();
    }

    async count(filter: QueryFilter): Promise<number> {

        const { q, name} = filter;

        const builder = this.contentTypeRepository
            .createQueryBuilder('contentType');

        if (q) {
            builder
                .where('contentType.name ~* :query')
                .setParameters({ query: `.*${q}.*` });
        }

        if(name) {
            builder
                .where('contentType.name = :name')
                .setParameters({ name })
        }

        return await builder.getCount();
    }

    async create(contentType: ContentTypeEntity): Promise<ContentTypeEntity> {
        return this.contentTypeRepository.save(contentType);
    }

    async findById(id: number): Promise<ContentTypeEntity | undefined> {
        return this.contentTypeRepository.findOne(id);
    }

    async updateById(id: number, contentType: ContentTypeEntity): Promise<ContentTypeEntity | undefined> {

        const count = await this.contentTypeRepository.count({ id });
        if (count === 0) return undefined;

        await this.contentTypeRepository.update(id, contentType);

        contentType.id = id;
        return contentType;
    }

    async deleteById(id: number): Promise<boolean> {

        const count = await this.contentTypeRepository.count({ id });

        if (count === 0) return false;

        await this.contentTypeRepository.delete(id, { });

        return true;
    }
}
