import {Injectable} from "@nestjs/common";

import * as Nodemailer from 'nodemailer';
import {SentMessageInfo} from 'nodemailer';

import Mail from "nodemailer/lib/mailer";
import {ConfigService} from '@app/shared/config.service';

@Injectable()
export class EmailService {

    private readonly transport: Mail;
    public readonly replyTo: string;

    constructor(configService: ConfigService) {
        this.transport = Nodemailer.createTransport(configService.config.get('smtp'));
        this.replyTo = configService.config.get('replyTo');
    }

    async send(options: Mail.Options): Promise<SentMessageInfo> {

        const defaults = {
            from: this.replyTo
        };

        const message = {...defaults, ...options};
        return this.transport.sendMail(message);
    }

}
