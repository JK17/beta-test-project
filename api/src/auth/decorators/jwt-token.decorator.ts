
import {createParamDecorator} from '@nestjs/common';

export const JwtToken = createParamDecorator((data, ctx) => {
    let user = ctx.user;
    if(!user && ctx.length >= 3) user = ctx[2].user;
    return user;                                        // handles normal and graphql contexts
});
