import {Injectable} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Repository} from 'typeorm';
import {QueryFilter} from '../shared/query-filter';
import {FormConfigEntity} from "@app/orm/entities/form-config.entity";

@Injectable()
export class FormConfigService {

    constructor(
        @InjectRepository(FormConfigEntity)
        private readonly formConfigRepository: Repository<FormConfigEntity>
    ){}

    async find(filter: QueryFilter): Promise<FormConfigEntity[]> {

        const { offset, limit, q, name} = filter;

        const builder = this.formConfigRepository
            .createQueryBuilder('formConfig');

        if (q) {
            builder
                .where('formConfig.name ~* :query')
                .setParameters({ query: `.*${q}.*` });
        }

        if(name) {
            builder
                .where('formConfig.name = :name')
                .setParameters({ name })
        }

        return await builder
            .offset(offset)
            .limit(limit)
            .orderBy('id', 'ASC')
            .getMany();
    }

    async count(filter: QueryFilter): Promise<number> {

        const { q, name} = filter;

        const builder = this.formConfigRepository
            .createQueryBuilder('formConfig');

        if (q) {
            builder
                .where('formConfig.name ~* :query')
                .setParameters({ query: `.*${q}.*` });
        }

        if(name) {
            builder
                .where('formConfig.name = :name')
                .setParameters({ name })
        }

        return await builder.getCount();
    }

    async create(formConfig: FormConfigEntity): Promise<FormConfigEntity> {
        return this.formConfigRepository.save(formConfig);
    }

    async findById(id: number): Promise<FormConfigEntity | undefined> {
        return this.formConfigRepository.findOne(id);
    }

    async updateById(id: number, formConfig: FormConfigEntity): Promise<FormConfigEntity | undefined> {

        const count = await this.formConfigRepository.count({ id });
        if (count === 0) return undefined;

        await this.formConfigRepository.update(id, formConfig);

        formConfig.id = id;
        return formConfig;
    }

    async deleteById(id: number): Promise<boolean> {

        const count = await this.formConfigRepository.count({ id });

        if (count === 0) return false;

        await this.formConfigRepository.delete(id, { });

        return true;
    }
}
