import {Module} from '@nestjs/common';
import {FormConfigEntity} from "@app/orm/entities/form-config.entity";
import {FormSubmissionEntity} from "@app/orm/entities/form-submission.entity";
import {TypeOrmModule} from '@nestjs/typeorm';
import {FormConfigService} from "@app/forms/form-config.service";
import {FormSubmissionService} from "@app/forms/form-submission.service";
import {FormConfigResolvers} from "@app/forms/form-config.resolvers";
import {FormSubmissionResolvers} from "@app/forms/form-submission.resolvers";
import {ContentModule} from "@app/content/content.module";
import {ContentService} from "@app/content/content.service";


@Module({
    imports: [
        TypeOrmModule.forFeature([
          FormConfigEntity,
          FormSubmissionEntity
        ]),
        ContentModule
    ],
    providers: [
        FormConfigService,
        FormSubmissionService,
        FormConfigResolvers,
        FormSubmissionResolvers,
        ContentService
    ]
})
export class FormsModule {

}
