import { assignClean } from '@app/shared/utils';
import {EmailConfig} from "@app/graphql/schema";


export class EmailConfigDto extends EmailConfig {

    constructor(data: any) {
        super();
        assignClean(this, data);
    }

}
