/* Replace with your SQL commands */

CREATE TABLE form_config
(
  id SERIAL PRIMARY KEY,
  name VARCHAR(64) NOT NULL UNIQUE,
  config JSONB NOT NULL,
  email_config JSONB NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);

CREATE TABLE form_submission
(
  id SERIAL PRIMARY KEY,
  form_config_id INT NOT NULL REFERENCES form_config,
  data JSONB NOT NULL,
  created_at TIMESTAMP NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP NOT NULL DEFAULT NOW()
);

INSERT INTO form_config
VALUES
  (1, 'contact', '{
    "fields": [
    {
      "fieldGroupClassName": "row",
      "fieldGroup": [
        {
        "key": "name",
        "type": "input",
        "className": "col-md-3",
        "templateOptions": {
          "placeholder": "Name",
          "required": true
        }
      },
      {
        "key": "surname",
        "type": "input",
        "className": "col-md-3",
        "templateOptions": {
          "placeholder": "Surname",
          "required": true
        }
      },
      {
        "key": "phone",
        "type": "input",
        "className": "col-md-3",
        "templateOptions": {
          "placeholder": "Phone",
          "required": true
        }
      },
      {
        "key": "email",
        "type": "input",
        "className": "col-md-3",
        "templateOptions": {
          "placeholder": "e-mail",
          "type": "email",
          "required": true
        }
      },
      {
        "key": "dob",
        "type": "datepicker",
        "className": "col-md-3",
        "templateOptions": {
          "placeholder": "Date of Birth",
          "required": true,
          "minDate": {
            "day": 1,
            "month": 1,
            "year": 1950
          }
        }
      },
      {
        "key": "gender",
        "type": "select",
        "className": "col-md-3",
        "templateOptions": {
          "placeholder": "Gender",
          "required": true,
          "select": {
            "items": [
              { "id":  "female", "name": "Female" },
              { "id":  "male", "name": "Male" }
            ]
          }
        }
      },
      {
        "key": "height",
        "type": "select",
        "className": "col-md-3",
        "templateOptions": {
          "placeholder": "Height",
          "required": true,
          "select": {
            "items": [
              { "id":  "165", "name": "165cm" },
              { "id":  "166", "name": "166cm" },
              { "id":  "167", "name": "167cm" },
              { "id":  "168", "name": "168cm" },
              { "id":  "169", "name": "169cm" },
              { "id":  "170", "name": "170cm" },
              { "id":  "171", "name": "171cm" },
              { "id":  "172", "name": "172cm" },
              { "id":  "173", "name": "173cm" },
              { "id":  "174", "name": "174cm" },
              { "id":  "175", "name": "175cm" },
              { "id":  "176", "name": "176cm" },
              { "id":  "177", "name": "177cm" },
              { "id":  "178", "name": "178cm" },
              { "id":  "179", "name": "179cm" },
              { "id":  "180", "name": "180cm" },
              { "id":  "181", "name": "181cm" },
              { "id":  "182", "name": "182cm" },
              { "id":  "183", "name": "183cm" },
              { "id":  "184", "name": "184cm" },
              { "id":  "185", "name": "185cm" },
              { "id":  "186", "name": "186cm" },
              { "id":  "187", "name": "187cm" },
              { "id":  "188", "name": "188cm" },
              { "id":  "189", "name": "189cm" },
              { "id":  "190", "name": "190cm" }
            ]
          }
        }
      },
      {
        "key": "shot1",
        "type": "file",
        "className": "col-md-3",
        "templateOptions": {
          "buttonText": "Shot 1",
          "required": true,
          "accept": "image/*"
        }
      },
      {
        "key": "shot2",
        "type": "file",
        "className": "col-md-3",
        "templateOptions": {
          "buttonText": "Shot 2",
          "required": true,
          "accept": "image/*"
        }
      },
      {
        "key": "shot3",
        "type": "file",
        "className": "col-md-3",
        "templateOptions": {
          "buttonText": "Shot 3",
          "required": true,
          "accept": "image/*"
        }
      },
      {
        "key": "shot4",
        "type": "file",
        "className": "col-md-3",
        "templateOptions": {
          "buttonText": "Shot 4",
          "required": true,
          "accept": "image/*"
        }
      },
      {
        "key": "shot5",
        "type": "file",
        "className": "col-md-3",
        "templateOptions": {
          "buttonText": "Shot 5",
          "required": true,
          "accept": "image/*"
        }
      }
      ]
    },
      {
        "key": "about",
        "type": "textarea",
        "templateOptions": {
          "label": "Tell us something about yourself",
          "required": true,
          "rows": 4
        }
      },
      {
        "key": "privacyNoticeAgreement",
        "type": "check-box",
        "id": "privacy-checkbox",
        "templateOptions": {
          "checkboxLabel": "By ticking this box you agree that you have read our privacy notice",
          "required": true
        }
      },
      {
        "key": "over16",
        "type": "check-box",
        "id": "age-checkbox",
        "templateOptions": {
          "checkboxLabel": "By ticking this box you agree that you are 16 years and older (if not you should seek parent/guardian/carer approval first).",
          "required": false
        }
      },
      {
        "key": "dataUsageAgreement",
        "type": "check-box",
        "id": "data-checkbox",
        "templateOptions": {
          "checkboxLabel": "By ticking this box you give your clear consent Berta Model Management will process your data only for the purpose of evaluating your potential as a (becoming a) model, and can use this data to contact you. Your application data will be kept here for no longer than 30 working days.",
          "required": true
        }
      }
    ]
  }', '{
         "emailRecipients": {
            "emails": ["berta@bertamodels.com"]
          },
          "subject": "Be a Model Form Submitted",
          "template": "<p>A new ''be a model'' form has been submitted with the following data:</p><ul style=\"list-style: none;\"><li>Name: {{name}} {{surname}}</li><li>Phone: {{phone}}</li><li>Email: {{email}}</li><li>Date of Birth: {{dob.day}}/{{dob.month}}/{{dob.year}}</li><li>Gender: {{gender}}</li><li>Height: {{height}}cm</li><li>About: {{about}}</li><li>Shots:<ul style=\"list-style: none;\"><li><img src=\"{{{shot1.metadata.secure_url}}}\"/> </li><li><img src=\"{{{shot2.metadata.secure_url}}}\"/> </li><li><img src=\"{{{shot3.metadata.secure_url}}}\"/> </li><li><img src=\"{{{shot4.metadata.secure_url}}}\"/> </li><li><img src=\"{{{shot5.metadata.secure_url}}}\"/> </li></ul></li></ul><p><a href=\"{{adminBaseUrl}}\">Berta Models</a></p>"

    }', NOW(), NOW()),
    (2, 'send-selection', '{
      "fields": [
        {
          "fieldGroupClassName": "row",
          "fieldGroup": [
            {
              "key": "senderEmail",
              "type": "input",
              "className": "col-md-6",
              "templateOptions": {
                 "placeholder": "Your e-mail",
                  "type": "email",
                   "required": true
              }
            },
            {
              "key": "recipientEmail",
              "type": "input",
              "className": "col-md-6",
              "templateOptions": {
                 "placeholder": "Recipient e-mail",
                  "type": "email",
                   "required": true
              }
            }
          ]
        }
      ]
    }', '{
      "emailRecipients": {
        "fields": ["recipientEmail"]
      },
      "subject": "Check out these models I''ve selected at Berta Models",
      "template": "<p>I''ve selected the following models at <a href=\"{{frontendBaseUrl}}\">Berta Models</a></p><ul style=\"list-style: none\">{{#models}}<li><a href=\"{{{frontendBaseUrl}}}/{{slug}}\"><h4>{{name}}</h4></a></li>{{#images}}<li><img src=\"{{{metadata.secure_url}}}\"</li>{{/images}}{{/models}}</ul>"
    }', NOW(), NOW());

ALTER SEQUENCE form_config_id_seq RESTART WITH 3;

INSERT INTO content_type
VALUES
  (5, 'form', null, '{
          "key": "formConfigId",
          "type": "select",
          "templateOptions": {
            "required": true,
            "label": "Form Config"
          }
        }', NOW(), NOW());

ALTER SEQUENCE content_type_id_seq RESTART WITH 6;

INSERT INTO "content" ("id", "title", "meta", "slug", "data", "content_type_id", "published_at", "created_at", "updated_at") VALUES
 (4, 'My Selection', '[{"key": "description", "value": "A list of my selected models"}]', 'my-selection', '{"formConfigId": 2, "formTitle": "Send your Selection"}', 4, NOW(), NOW(), NOW()),
 (5, 'Be A Model', '[{"key": "description", "value": "A form to apply to be a model"}]', 'be-a-model', '{"formConfigId": 1, "title": "Be a model", "subtitle": "If you think you have what it takes to be a model, fullfill this form. If we’re interested we will get in touch with you.", "buttonText": "Send", "submitted": "Thanks!"}', 5, NOW(), NOW(), NOW());

ALTER SEQUENCE content_id_seq RESTART WITH 6;