/* Replace with your SQL commands */

CREATE TABLE content_type (
  id         SERIAL PRIMARY KEY,
  name       VARCHAR(64) NOT NULL UNIQUE,
  slug       VARCHAR(64) UNIQUE,
  form_config JSONB NOT NULL,
  created_at TIMESTAMP   NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMP   NOT NULL DEFAULT NOW()
);

CREATE TABLE content (
  id              SERIAL PRIMARY KEY,
  title           VARCHAR(64) NOT NULL,
  meta            JSONB NOT NULL,
  slug            VARCHAR(64) NOT NULL,
  data            JSONB NOT NULL,
  content_type_id INT         NOT NULL REFERENCES content_type,
  published_at    TIMESTAMP   DEFAULT NULL,
  created_at      TIMESTAMP   NOT NULL DEFAULT NOW(),
  updated_at      TIMESTAMP   NOT NULL DEFAULT NOW()
);

CREATE TABLE file (
  id           SERIAL PRIMARY KEY,
  owner_id     INT          REFERENCES "user",
  mimetype     VARCHAR(128) NOT NULL,
  filename     VARCHAR(128) NOT NULL,
  cl_name      VARCHAR(32)  NOT NULL,
  cl_public_id VARCHAR(32)  NOT NULL,
  metadata     JSONB NULL,
  created_at   TIMESTAMP    NOT NULL DEFAULT NOW(),
  updated_at   TIMESTAMP    NOT NULL DEFAULT NOW()
);

INSERT INTO content_type
VALUES (1, 'model', null, '{
      "fields": [
          {
            "key": "name",
            "type": "input",
            "templateOptions": {
              "label": "Name",
              "required": true
            }
          },
          {
            "key": "email",
            "type": "input",
            "templateOptions": {
              "label": "Email",
              "required": true
            }
          },
          {
            "key": "gender",
            "type": "select",
            "templateOptions": {
              "label": "Gender",
              "required": true,
              "select": {
                "items": [
                   { "id":  "female", "name": "Female" },
                    { "id":  "male", "name": "Male" }
                ]
              }
            }
          },
           {
            "key": "country",
            "type": "input",
            "templateOptions": {
              "label": "Country",
              "required": true
            }
          },
           {
            "key": "inTown",
            "type": "checkbox",
            "templateOptions": {
              "label": "In Town"
            }
          },
          {
            "key": "height",
            "type": "input",
            "templateOptions": {
              "label": "Height",
              "type": "number",
              "required": true
            }
          },
          {
            "key": "bust",
            "type": "input",
            "templateOptions": {
              "label": "Bust",
              "type": "number",
              "required": true
            }
          },
          {
            "key": "waist",
            "type": "input",
            "templateOptions": {
              "label": "Waist",
              "type": "number",
              "required": true
            }
          },
          {
            "key": "hips",
            "type": "input",
            "templateOptions": {
              "label": "Hips",
              "type": "number",
              "required": true
            }
          },
          {
            "key": "shoes",
            "type": "input",
            "templateOptions": {
              "label": "Shoes",
              "type": "number",
              "required": true
            }
          },
          {
            "key": "eyeColor",
            "type": "input",
            "templateOptions": {
              "label": "Eye Color",
              "required": true
            }
          },
          {
            "key": "hairColor",
            "type": "input",
            "templateOptions": {
              "label": "Hair",
              "required": true
            }
          },
          {
            "key": "instagram",
            "type": "input",
            "templateOptions": {
              "label": "Instagram",
              "required": true
            }
          },
          {
              "key": "images",
              "type": "repeat",
              "fieldArray": {
                "fieldGroupClassName": "row",
                "templateOptions": {
                  "btnText": "Add Image"
                },
                "fieldGroup": [
                 {
                    "key": "id",
                    "type": "file",
                    "templateOptions": {
                      "required": true,
                      "accept": "image/*",
                      "noClose": true
                    }
                  }
                ]
            }
        }
      ]
      }', NOW(), NOW()),
       (2, 'models-in-town', null, '{}', NOW(), NOW()),
       (3, 'models-by-gender', null, '{
          "key": "gender",
          "type": "input",
          "templateOptions": {
            "required": true,
            "label": "Gender"
          }
        }', NOW(), NOW()),
        (4, 'my-selection', null, '{
          "fields": [
            {
              "key": "formConfigId",
              "type": "input",
              "templateOptions": {
                "required": true,
                "label": "Form Type",
                "type": "number"
              }
            },
            {
              "key": "formTitle",
              "type": "input",
              "templateOptions": {
                "required": false,
                "label": "Form Title"
              }
            }
          ]
        }', NOW(), NOW());

ALTER SEQUENCE content_type_id_seq RESTART WITH 5;

INSERT INTO "content" ("id", "title", "meta", "slug", "data", "content_type_id", "published_at", "created_at", "updated_at") VALUES
(1, 'In Town', '[{"key":"description","value": "Models currently in Barcelona"}]', '', '{}', 2, NOW(), NOW(), NOW()),
(2, 'Women', '[{"key": "description", "value": "An alphabetical list of all female models"}]', 'women', '{"gender":"female"}', 3, NOW(), NOW(), NOW()),
(3, 'Men', '[{"key": "description", "value": "An alphabetical list of all male models"}]', 'men', '{"gender":"male"}', 3, NOW(), NOW(), NOW());

ALTER SEQUENCE content_id_seq RESTART WITH 4;
