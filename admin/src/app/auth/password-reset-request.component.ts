import {Component} from '@angular/core';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormGroup, Validators} from '@angular/forms';
import {BaseComponent} from '../shared/base.component';
import {Apollo} from 'apollo-angular';
import {requestPasswordReset} from "./auth.graphql";


@Component({
  selector: 'app-password-reset-request',
  styles: [`
    form {

      max-width: 330px;
      width: 100%;
      padding: 15px;
      margin: 200px auto auto auto;

    }
  `],
  template: `
    <div>
      
      <form [formGroup]="form" (ngSubmit)="onSubmit()">
        <ngb-alert *ngIf="infoText" type="info" [dismissible]="false">
          {{ infoText }}
        </ngb-alert>
        <ngb-alert *ngIf="errorText" type="danger" [dismissible]="false">
          {{ errorText }}
        </ngb-alert>
        <formly-form [form]="form" [fields]="fields" [model]="model">
          <button type="submit" class="btn btn-primary btn-block" [disabled]="form.invalid">Request Reset</button>
        </formly-form>
        <a routerLink="/login">Back to login</a>
      </form>
      
    </div>
  `
})
export class PasswordResetRequestComponent extends BaseComponent {

  fields: FormlyFieldConfig[] = [{
    type: 'input',
    key: 'email',
    templateOptions: {
      placeholder: 'Email',
      required: true,
    },
    validators: {
      validation: [Validators.email],
    }
  }];

  form = new FormGroup({});

  model: any = {};

  infoText?: string;
  errorText?: string;

  constructor(private readonly apollo: Apollo) {
    super();
  }

  onSubmit() {

    this.errorText = undefined;
    this.infoText = undefined;

    this.apollo.mutate({
      mutation: requestPasswordReset,
      variables: this.model
    }).subscribe(
      ({data}) => {

        const {requestPasswordReset} = data;

        if(requestPasswordReset) {
          this.infoText = 'An email has been sent with further instructions.'
        } else {
          this.errorText = 'We have no record of this email address. Please check you have entered it correctly and try again.';
        }
      },
      (error) => {

        this.errorText = 'Unexpected Server error. Please contact the support team';

      });

  }

}
