import {Component, OnInit} from '@angular/core';
import {FormlyFieldConfig} from '@ngx-formly/core';
import {FormGroup} from '@angular/forms';
import {BaseComponent} from '@app/shared/base.component';
import {Apollo} from 'apollo-angular';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {resetPassword} from "@app/auth/auth.graphql";
import {ToastrService} from "ngx-toastr";


@Component({
  selector: 'app-reset-password',
  styles: [`
    form {

      max-width: 330px;
      width: 100%;
      padding: 15px;
      margin: 200px auto auto auto;

    }
  `],
  template: `
    <div>

      <form [formGroup]="form" (ngSubmit)="onSubmit()">
        <ngb-alert *ngIf="infoText" type="info" [dismissible]="false">
          {{ infoText }}
        </ngb-alert>
        <ngb-alert *ngIf="errorText" type="danger" [dismissible]="false">
          {{ errorText }}
        </ngb-alert>
        <formly-form [form]="form" [fields]="fields" [model]="model">
          <button type="submit" class="btn btn-primary btn-block" [disabled]="form.invalid">Reset Password</button>
        </formly-form>
        <a routerLink="/login">Back to login</a>
      </form>

    </div>
  `
})
export class ResetPasswordComponent extends BaseComponent implements OnInit {

  fields: FormlyFieldConfig[] = [{
    key: 'password',
    validators: {
      fieldMatch: {
        expression: (control) => {
          const value = control.value;

          return value.passwordConfirm === value.password
            // avoid displaying the message error when values are empty
            || (!value.passwordConfirm || !value.password);
        },
        message: 'Password Not Matching',
        errorPath: 'passwordConfirm',
      },
    },
    fieldGroup: [
      {
        key: 'password',
        type: 'input',
        templateOptions: {
          type: 'password',
          label: 'Password',
          placeholder: 'Must be at least 3 characters',
          required: true,
          minLength: 3,
        },
      },
      {
        key: 'passwordConfirm',
        type: 'input',
        templateOptions: {
          type: 'password',
          label: 'Confirm Password',
          placeholder: 'Please re-enter your password',
          required: true,
        },
      },
    ],
  }];

  form = new FormGroup({});

  model: any = {};

  infoText?: string;
  errorText?: string;

  constructor(private readonly apollo: Apollo,
              private readonly router: Router,
              private readonly route: ActivatedRoute,
              private readonly toastrService: ToastrService) {
    super();
  }

  ngOnInit(): void {

    // pick up access token from query params and set in the apollo store

    this.subscription(
      () => this.route.queryParams
        .subscribe((params: Params) => {

          const {access_token} = params;
          const expiresIn = 3600 * 2;

          return this.apollo.getClient()
            .writeData({
              data: {
                accessToken: {
                  id: access_token, expiresIn
                }
              }
            });

        })
    );

  }

  onSubmit() {

    const {password} = this.model.password;

    this.apollo.mutate({
      mutation: resetPassword,
      variables: {password}
    }).subscribe(
      ({data}) => {

        const {resetPassword} = data;

        if (resetPassword) {
          this.toastrService.success('Password successfully reset');
          this.router.navigateByUrl('/login');
        }

      },
      (error) => {

        this.errorText = 'Unexpected Server error. Please contact the support team';

      });

  }

}
