import {NgModule} from '@angular/core';
import {APOLLO_OPTIONS, ApolloModule} from 'apollo-angular';
import {HttpLink, HttpLinkModule} from 'apollo-angular-link-http';
import {InMemoryCache} from 'apollo-cache-inmemory';
import {withClientState} from 'apollo-link-state';
import {persistCache} from 'apollo-cache-persist';
import {environment} from '@src/environments/environment';


export function createApollo(httpLink: HttpLink) {

  const cache = new InMemoryCache();

  persistCache({ cache, storage: window.localStorage });

  const local = withClientState({
    cache,
    resolvers: {},
    defaults: {}
  });

  let http = httpLink.create({ uri: environment.apiUrl + '/graphql'});

  return {
    cache,
    link: local.concat(http)
  };

}

@NgModule({
  imports: [
    ApolloModule,
    HttpLinkModule
  ],
  providers: [
    {
      provide: APOLLO_OPTIONS,
      useFactory: createApollo,
      deps: [HttpLink]
    }
  ]
})
export class CmsApolloModule {
}
