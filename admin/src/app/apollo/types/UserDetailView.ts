/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: UserDetailView
// ====================================================

export interface UserDetailView {
  __typename: "User";
  id: string | null;
  email: string | null;
  firstName: string | null;
  familyName: string | null;
  createdAt: any | null;
  updatedAt: any | null;
}
