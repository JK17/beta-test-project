/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ContentTypeListView
// ====================================================

export interface ContentTypeListView {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
  createdAt: any | null;
  updatedAt: any | null;
}
