/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ContentDetailById
// ====================================================

export interface ContentDetailById_contentAdmin_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
}

export interface ContentDetailById_contentAdmin {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  publishedAt: any | null;
  contentType: ContentDetailById_contentAdmin_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ContentDetailById {
  contentAdmin: ContentDetailById_contentAdmin | null;
}

export interface ContentDetailByIdVariables {
  id: string;
}
