/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: ContentDetailView
// ====================================================

export interface ContentDetailView_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
  slug: string | null;
  formConfig: any | null;
}

export interface ContentDetailView {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  publishedAt: any | null;
  contentType: ContentDetailView_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}
