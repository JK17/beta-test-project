/* tslint:disable */
// This file was automatically generated and should not be edited.

import { UpsertFile } from "./globalTypes";

// ====================================================
// GraphQL mutation operation: UpdateFileById
// ====================================================

export interface UpdateFileById_updateFileById_owner {
  __typename: "User";
  id: string | null;
}

export interface UpdateFileById_updateFileById {
  __typename: "File";
  id: string | null;
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
  createdAt: any | null;
  updatedAt: any | null;
  owner: UpdateFileById_updateFileById_owner | null;
  clTransforms: string | null;
}

export interface UpdateFileById {
  updateFileById: UpdateFileById_updateFileById;
}

export interface UpdateFileByIdVariables {
  id: string;
  file?: UpsertFile | null;
}
