/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: FileDetail
// ====================================================

export interface FileDetail_file_owner {
  __typename: "User";
  id: string | null;
}

export interface FileDetail_file {
  __typename: "File";
  id: string | null;
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
  createdAt: any | null;
  updatedAt: any | null;
  owner: FileDetail_file_owner | null;
  clTransforms: string | null;
}

export interface FileDetail {
  file: FileDetail_file | null;
}

export interface FileDetailVariables {
  id: string;
}
