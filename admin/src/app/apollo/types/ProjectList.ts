/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: ProjectList
// ====================================================

export interface ProjectList_contents_contentType {
  __typename: "ContentType";
  id: string | null;
  name: string | null;
}

export interface ProjectList_contents {
  __typename: "Content";
  id: string | null;
  title: string | null;
  meta: any | null;
  slug: string | null;
  data: any | null;
  contentType: ProjectList_contents_contentType | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface ProjectList {
  contents: (ProjectList_contents | null)[];
  contentsCount: number;
}

export interface ProjectListVariables {
  offset?: number | null;
  limit?: number | null;
  q?: string | null;
}
