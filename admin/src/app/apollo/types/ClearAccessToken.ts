/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: ClearAccessToken
// ====================================================

export interface ClearAccessToken {
  clearAccessToken: boolean | null;
}
