/* tslint:disable */
// This file was automatically generated and should not be edited.

//==============================================================
// START Enums and Input Objects
//==============================================================

export enum PublishState {
  ALL = "ALL",
  DRAFT = "DRAFT",
  PUBLISHED = "PUBLISHED",
}

export interface UpdateSetting {
  id?: string | null;
  value?: string | null;
}

export interface UpsertContent {
  title?: string | null;
  meta?: any | null;
  slug?: string | null;
  contentTypeId?: string | null;
  data?: any | null;
  publishedAt?: any | null;
  modelContent?: UpsertModelContent | null;
}

export interface UpsertFile {
  clTransforms?: string | null;
}

export interface UpsertModelContent {
  name?: string | null;
  email?: string | null;
  gender?: string | null;
  country?: string | null;
  inTown?: boolean | null;
  height?: number | null;
  bust?: number | null;
  waist?: number | null;
  hips?: number | null;
  shoes?: number | null;
  eyeColor?: string | null;
  hair?: string | null;
  instagram?: string | null;
}

export interface UpsertUser {
  email?: string | null;
  firstName?: string | null;
  familyName?: string | null;
  password?: string | null;
}

//==============================================================
// END Enums and Input Objects
//==============================================================
