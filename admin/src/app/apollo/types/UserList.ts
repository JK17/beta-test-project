/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: UserList
// ====================================================

export interface UserList_users {
  __typename: "User";
  id: string | null;
  email: string | null;
  firstName: string | null;
  familyName: string | null;
  createdAt: any | null;
  updatedAt: any | null;
}

export interface UserList {
  users: (UserList_users | null)[];
  usersCount: number;
}

export interface UserListVariables {
  offset?: number | null;
  limit?: number | null;
  q?: string | null;
}
