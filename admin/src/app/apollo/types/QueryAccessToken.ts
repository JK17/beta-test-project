/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL query operation: QueryAccessToken
// ====================================================

export interface QueryAccessToken_accessToken {
  __typename: "AccessToken";
  id: string | null;
}

export interface QueryAccessToken {
  accessToken: QueryAccessToken_accessToken | null;
}
