/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: FileDetailView
// ====================================================

export interface FileDetailView_owner {
  __typename: "User";
  id: string | null;
}

export interface FileDetailView {
  __typename: "File";
  id: string | null;
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
  createdAt: any | null;
  updatedAt: any | null;
  owner: FileDetailView_owner | null;
  clTransforms: string | null;
}
