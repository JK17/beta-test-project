/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: AuthenticateWithEmail
// ====================================================

export interface AuthenticateWithEmail_authenticateWithEmail {
  __typename: "AccessToken";
  id: string | null;
  expiresIn: number | null;
}

export interface AuthenticateWithEmail {
  authenticateWithEmail: AuthenticateWithEmail_authenticateWithEmail | null;
}

export interface AuthenticateWithEmailVariables {
  email: string;
  password: string;
}
