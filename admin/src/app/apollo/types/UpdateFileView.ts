/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL fragment: UpdateFileView
// ====================================================

export interface UpdateFileView {
  __typename: "File";
  mimetype: string | null;
  filename: string | null;
  metadata: any | null;
  clTransforms: string | null;
}
