/* tslint:disable */
// This file was automatically generated and should not be edited.

// ====================================================
// GraphQL mutation operation: DeleteContentById
// ====================================================

export interface DeleteContentById {
  deleteContentById: boolean | null;
}

export interface DeleteContentByIdVariables {
  id: string;
}
