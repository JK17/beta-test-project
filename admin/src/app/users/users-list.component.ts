import {Component} from '@angular/core';
import {BaseComponent} from '../shared/base.component';
import {deleteUserByIdMutation, userDetailQuery, userListQuery} from './users.graphql';

@Component({
  selector: 'app-users-list',
  template: `
    <app-list-view [listQuery]="listQuery"
                   [detailQuery]="detailQuery"
                   [deleteByIdMutation]="deleteByIdMutation"
                   [modelsParser]="modelsParser"
                   [countParser]="countParser">

      <ng-container *listHeader>
        <th scope="col" class="col-sm-1">Id</th>
        <th scope="col" class="col-sm-2">Email</th>
        <th scope="col" class="col-sm-2">Name</th>
        <th scope="col" class="col-sm-2">Family Name</th>
        <th scope="col" class="col-sm-2">Created</th>
        <th scope="col" class="col-sm-2">Last Updated</th>
        <th scope="col" class="col-sm-1">Actions</th>
      </ng-container>

      <ng-container *listItem="let model;">
        <td class="col-sm-1">{{ model.id }}</td>
        <td class="col-sm-2">{{ model.email }}</td>
        <td class="col-sm-2">{{ model.firstName }}</td>
        <td class="col-sm-2">{{ model.familyName }}</td>
        <td class="col-sm-2">{{ model.createdAt | timeago:live }}</td>
        <td class="col-sm-2">{{ model.updatedAt | timeago:live }}</td>
      </ng-container>

    </app-list-view>
  `
})
export class UsersListComponent extends BaseComponent {

  listQuery = userListQuery;
  detailQuery = userDetailQuery;
  deleteByIdMutation = deleteUserByIdMutation;

  modelsParser(data: any): any[] {
    return data.users;
  }

  countParser(data: any): number {
    return data.usersCount;
  }

}
