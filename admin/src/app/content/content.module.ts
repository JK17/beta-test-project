import { NgModule } from '@angular/core';
import { ContentRoutesModule } from './content.routes';
import { ContentTypeListComponent } from './content-type-list.component';
import { LayoutModule } from '../layout/layout.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { CommonModule } from '@angular/common';
import { NgbPaginationModule } from '@ng-bootstrap/ng-bootstrap';
import { FormlyModule } from '@ngx-formly/core';
import { ContentDetailComponent } from './content-detail.component';
import { ImageCropperComponent } from './image-cropper.component';
import { TimeagoModule } from 'ngx-timeago';
import { IconsModule } from '../icons.module';
import { CrudModule } from '../crud/crud.module';
import { ModelListComponent } from '@app/content/model-list.component';
import { AngularCropperjsModule } from 'angular-cropperjs';


@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    ReactiveFormsModule,
    NgbPaginationModule,
    FormlyModule,
    ContentRoutesModule,
    TimeagoModule,
    IconsModule,
    CrudModule,
    AngularCropperjsModule,
    FormsModule
  ],
  declarations: [
    ContentDetailComponent,
    ContentDetailComponent,
    ContentTypeListComponent,
    ModelListComponent,
    ImageCropperComponent,
  ]
})
export class ContentModule {
}
