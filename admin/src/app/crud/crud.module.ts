import {NgModule} from '@angular/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {ListViewComponent} from './list-view.component';
import {ListItemDirective} from './list-item.directive';
import {ListHeaderDirective} from './list-header.directive';
import {RouterModule} from '@angular/router';
import {IconsModule} from '../icons.module';
import {NgbPaginationModule} from '@ng-bootstrap/ng-bootstrap';
import {FormValidationService} from './form-validation.service';
import {NgSelectModule} from "@ng-select/ng-select";


@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    RouterModule,
    IconsModule,
    NgbPaginationModule,
    NgSelectModule
  ],
  declarations: [
    ListViewComponent,
    ListItemDirective,
    ListHeaderDirective
  ],
  exports: [
    ListViewComponent,
    ListItemDirective,
    ListHeaderDirective
  ],
  providers: [
    FormValidationService
  ]
})
export class CrudModule {

}
