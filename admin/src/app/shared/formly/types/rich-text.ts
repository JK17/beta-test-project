import {Component} from '@angular/core';
import {FieldType} from '@ngx-formly/core';

@Component({
  selector: 'formly-rich-text',
  template: `
    <quill-editor [maxLength]="to.maxLength"
                  [minLength]="0" 
                  [modules]="to.modules"
                  [readOnly]="to.disabled"
                  [style]="{height: '250px'}"
                  [formControl]="formControl">
    </quill-editor>
  `,
})
export class FormlyRichText extends FieldType {


  ngOnInit(): void {

    this.to.modules = this.to.modules || {

    };

    super.ngOnInit();
  }
}
